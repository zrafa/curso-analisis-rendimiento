#include <stdio.h>
#include <stdlib.h>
#include <time.h>

#define Cx 0.1f
#define Cy 0.1f

double sampleTime(void);
double **reservaMatriz(long filas, long columnas);
void saveFile(char *filename, double **matriz, long filas, long columnas);
void inicializarMatriz(double **matrizActual, int Tlado, long filas, long columnas);

double kernelSerial(double **matrizActual, int Tlado, int pasos, long filas, long columnas);

int main(int argc, char *argv[]) {
   if(argc != 3) {
      printf("Error de ejecución! \nUtilice el programa de la siguiente forma:\n\n");
      printf("%s tamaño pasos\n\n", argv[0]);
      exit(1);
   }
   int Tlado = atoi(argv[1]); //N --> Tamaño de la matriz cuadrada
   int pasos = atoi(argv[2]); //M --> Cantidad de pasos
   if (Tlado <= 0 || pasos < 0) {
      printf("ERROR: valores incorrectos en parámetros\n");
      exit(1);
   }

   int filas = Tlado + 2;
   int columnas = Tlado + 2;

   double **matrizActual;

   matrizActual = reservaMatriz(filas, columnas);

   //Inicialización de la matriz Actual
   inicializarMatriz(matrizActual, Tlado, filas, columnas);

   saveFile("grid_inicial.out", matrizActual, filas, columnas);

   //Ejecución de los pasos de simulación
   double time_spent;
   time_spent = kernelSerial(matrizActual, Tlado, pasos, filas, columnas);

   printf("Tiempo de ejecución: %.5f segundos.\n", time_spent);

   //Se almacena la matriz final en un archivo
   saveFile("grid_serie.out", matrizActual, filas, columnas);
   
   return 0;
}

double sampleTime(void) {
   struct timespec tv;
   clock_gettime(CLOCK_MONOTONIC, &tv);
   return ((double) tv.tv_sec + ((double)tv.tv_nsec) / 1000000000.0);
}

double **reservaMatriz(long filas, long columnas){
  register long i;
  double **matriz;
  //Reserva de espacio para la matriz
  matriz = (double **)malloc(sizeof(double *) * filas);
  *matriz = (double *)malloc(sizeof(double) * filas * columnas);

  if (matriz == NULL || *matriz == NULL){
    printf("ERROR: No se pudo reservar memoria para matriz\n");
    exit(2);
  }

  //Se calculan las direcciones de las filas
  for(i = 1; i < filas; i++)
    matriz[i] = *matriz + columnas * i;
    
  return matriz;
}

void saveFile(char *filename, double **matriz, long filas, long columnas){
   register long i = 0, j = 0;
   printf("Guardando archivo %s...\n", filename);
   FILE *f = fopen(filename, "w");
   if (f == NULL) {
      printf("ERROR: No se pudo abrir el archivo\n");
      exit(1);
   }
   for (i = 1; i <  filas - 1; i++) {
      for (j = 1; j < columnas - 1; j++)
         fprintf(f, "%8.3f ", matriz[i][j]);
      fprintf(f, "\n");
   }
   fclose(f);
}

void inicializarMatriz(double **matrizActual, int Tlado, long filas, long columnas){
   // Completar aquí...
   // Esta función debe recorrer la matriz inicializando cada elemento según la
   // fórmula 2 del enunciado

	int i,x,y;
	double **m = matrizActual;

	/* inicializamos contorno de la matriz */
	for (i = 1; i < columnas - 1; i++) {
       		m[0][i] = 0;
       		m[filas-1][i] = 0;
       		m[i][0] = 0;
       		m[i][columnas-1] = 0;
	}

	for (y = 1; y <  filas - 1; y++)
		for (x = 1; x < columnas - 1; x++)
			m[y][x] = x * (Tlado - x - 1) * y * (Tlado - y - 1) / (4 * Tlado * Tlado);

	
}

double kernelSerial(double **matrizActual, int Tlado, int pasos, long filas, long columnas){
	int i, x, y;
	double **m1, **m2, **m3;
	
	double t;
	t = sampleTime();

	m2 = reservaMatriz(filas, columnas);
	m1 = matrizActual;

	for (i=0; i<pasos; i++) {

		for (y = 1; y <  filas - 1; y++)
	 		for (x = 1; x < columnas - 1; x++)
				m2[y][x] = m1[y][x] + Cx * (m1[y][x+1] + m1[y][x-1] - 2 * m1[y][x]) + Cy * (m1[y+1][x] + m1[y-1][x] - 2 * m1[y][x]);
		m3 = m1;
		m1 = m2;
		m2 = m3;
	}

	t = sampleTime() - t;

   // Completar aquí...
   // Esta función debe:
   // - Reservar memoria para una matriz adicional en la que se guarden los cálculos
   //   de cada paso de simulación.
   // - Por cada paso de simulación, leer datos de matrizActual, computar el valor de 
   //   la fórmula 1 para cada elemento y actualizar la matriz adicional. Al final de cada
   //   paso de simulación, debe intercambiar las matrices actual y siguiente de
   //   modo que el siguiente paso de simulación realice sus cálculos sobre los 
   //   nuevos valores.
   // - Devolver la duración en segundos del kernel.
}
