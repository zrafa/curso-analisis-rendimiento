#!/bin/bash
height=40000
width=2000
listOfCores="1 $(seq 4 4 40)"

for cores in $listOfCores; do
   srun -N1 -n1 -c$cores --exclusive ./run_omp.sh ./julia $height $width  
done
