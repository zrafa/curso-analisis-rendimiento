#!/bin/bash
height=$1
width=$2
listOfCores="1 $(seq 4 4 40)"

for cores in $listOfCores; do
        srun -N1 -n1 -c$cores --exclusive ./run_omp.sh ./julia $((cores*10000)) $width  
done

